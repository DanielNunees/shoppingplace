import { ProductComponent } from './view/product/list-products/product.component';
import { NotFoundComponent } from './view/404-page/notFound.component';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AuthenticationGuard } from './guards/authentication.guard';
import { LoginComponent } from "./view/login/login.component";


const appRoutes: Routes = [
    { path: '', redirectTo:'product/list', pathMatch:'full', canActivate:[AuthenticationGuard] },
    
    { path: 'login', component: LoginComponent },
    ];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }