import { XHRBackend, RequestOptions } from '@angular/http';
import { HttpTalkerService } from './service/http-talker/http-talker-service';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { NotFoundModule } from './view/404-page/notFound.module';
import { MainModule } from './view/main/main.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

import { LoginModule } from './view/login/login.module';
import { NgxSpinnerModule, NgxSpinnerService } from 'ngx-spinner';
import { httpTalkServiceFactory } from './factories/http-talk-service-factory';




@NgModule({
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        LoginModule,
        NotFoundModule,
        MainModule,
        NgxSpinnerModule,
        SweetAlert2Module.forRoot()
    ],
    declarations: [
        AppComponent,
    ],
    providers: [
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        NgxSpinnerService,{
            provide: HttpTalkerService,
            useFactory: httpTalkServiceFactory,
            deps: [XHRBackend, RequestOptions, NgxSpinnerService ]  
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
