import { Images, ProductModel } from './product.model';
export class ProductCombinationCardData {
    index: number
    color: string;
    combination: ProductModel[];

    constructor(color?, combination?, index?) {
        this.color = color;
        this.combination = combination;
        this.index = index;
    }
}