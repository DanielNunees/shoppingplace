import { Sizes, Categories, Colors, Brands } from "./product.model";

/**
 * Classe com os atributos enviados pelo servidor com as informações necessaris para criar ou editar um produto
 */
export class ProductData {
    colors: Colors[];
    brands: Brands[];
    categories: Categories[];
    sizes: Sizes[];

}