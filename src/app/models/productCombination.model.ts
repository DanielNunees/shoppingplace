import { Images } from './product.model';
export class ProductCombination {
    product_id: Number;
    id: Number; // productCombinationId
    size_id: Number;
    color_id: Number;
    brand_id: Number;
    price: Number;
    category_id: Number;
    image_id: Number
    quantity: Number;
    images: Images[];
    updated_at: Date;
    created_at: Date;

    constructor(product_id, product_cobination_id, size_id, color_id,brand_id, price, quantity, images, created_at) {
        this.product_id = product_id;
        this.size_id = size_id;
        this.color_id = color_id;
        this.brand_id = brand_id;
        this.quantity = quantity;
        this.images = images;
        this.price = price;
        this.id = product_cobination_id;
        this.created_at = created_at;
    }
}