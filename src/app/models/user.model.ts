/**
 * Classe que representa um Usuário do sistema.
 */
export class UserModel {
  idUser: number;
  username: string;
  name: string;
  password: string;
  domain: string;
  lastAccess: Date;
  status: string;
  email: string;
  lockDate: Date;
  department: string;
  statusName: string;
}