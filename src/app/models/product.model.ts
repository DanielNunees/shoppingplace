export class ProductModel {
    id: number;
    name: String;
    description: String;
    price: number;
    quantity: number;
    sizes: Sizes[];
    size: Sizes;
    images: Images[];
    image: String;
    colors: Colors;
    color: Colors;
    category: String;
    brands: Brands[];
    brand_id: number;
    categories: Categories[];
    SKU: String;
    ranking: Number;
    shopId: Number;
    active: Number;
    created_at: Date;
    updated_at: Date;
}

export class Categories {
    id: Number;
    name: String;
    description: String;
    image_id: number;
    active: Number;
}

export class Colors {
    id: number;
    color: String;
}

export class Images {
    color_id: Number;
    src: String;

    constructor(src) {
        this.src = src;
    }
}

export class Sizes {
    id: number;
    size: string;
}

export class Brands {
    id: Number;
    name: string;
}