/**
 * Classe com os atributos enviados pelo servidor logo após o login do usuário ser realizado com sucesso.
 */
export class ProfileModel {
    username: string;
    name: string;
    domain: string;
    email: string;
    token: string;
    department: string;
    permissions: string[];
    version: string;
    device: string;
    lastAccess: Date;
    remoteAddr: string;
}