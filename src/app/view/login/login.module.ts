import { AuthenticationGuard } from './../../guards/authentication.guard';
import { AuthService } from './../../service/authentication/auth.service';
import { LoginComponent } from './login.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule } from '@angular/forms';
import { ProgressSpinnerModule, BlockUIModule } from 'primeng/primeng';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
//PrimeNg Modules


@NgModule({
    imports: [
        CommonModule,
        InputTextModule,
        FormsModule,
        ProgressSpinnerModule,
        BlockUIModule,
        SweetAlert2Module
    ],
    declarations: [
        LoginComponent
    ],
    providers: [
        AuthService,
        AuthenticationGuard
    ],
})
export class LoginModule { }
