import { Router } from '@angular/router';
import { AuthService } from './../../service/authentication/auth.service';
import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';
import swal from "sweetalert2";

export class LoginDataForm {
    username: string;
    password: string;
}

@Component({
    selector: 'login',
    templateUrl: 'login.component.html'

})


export class LoginComponent implements OnInit {

    private user: LoginDataForm = new LoginDataForm();
    constructor(private authService: AuthService,
        private router: Router ) {

    }

    ngOnInit(): void {

    }

    login() {
        this.authService.login(this.user).subscribe(
            res => {
                this.router.navigate(['dashboard']);
            },
            error => {
                swal('Erro!', "Senha ou Usuário inválidos!", 'error');
            }
        );
    }




}