//import { NotFoundRoutingModule } from './notFound.routing.module';
import { NotFoundComponent } from './notFound.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
//PrimeNg Modules


@NgModule({
    imports: [
        CommonModule,
        //NotFoundRoutingModule
    ],
    declarations: [
        NotFoundComponent
    ],
    providers: [],
})
export class NotFoundModule { }
