import { NotFoundComponent } from './../404-page/notFound.component';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { AuthenticationGuard } from './../../guards/authentication.guard';

const NotFoundRoutes: Routes = [{path: '**', redirectTo: '404'}];



@NgModule({
    imports: [RouterModule.forChild(NotFoundRoutes)],
    exports: [RouterModule]
})
export class Not_FoundRoutingModule { }

