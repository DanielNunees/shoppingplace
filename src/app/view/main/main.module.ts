import { NgxSpinnerService, NgxSpinnerModule } from 'ngx-spinner';
import { DashboardModule } from './../dashboard/dashboard.module';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { BreadCrumbService } from './breadcrumb.service';
import 'rxjs/add/operator/toPromise';

//PrimeNg Modules
import { TableModule } from 'primeng/table';
import { DataTableModule } from 'primeng/datatable';
import { ScrollPanelModule } from 'primeng/scrollpanel';


import { MainComponent } from './main.component';
import { MainMenuComponent, MainSubMenuComponent } from './main.menu.component';
import { MainTopbarComponent } from './main.topbar.component';
import { MainFooterComponent } from './main.footer.component';
import { BreadcrumbComponent } from './breadcrumb.component';
import { MainRightpanelComponent } from './main.rightpanel.component';
import { MainInlineProfileComponent } from './main.profile.component';

//New Components
import { ProductModule } from '../../view/product/product.module';

//New Services
import { ProductService } from '../../service/product/product.service';
import { LocalStorageService } from '../../service/local-storage/local-storage.service';
import { AuthenticationGuard } from '../../guards/authentication.guard';

import { AuthService } from '../../service/authentication/auth.service';
import { SessionService } from '../../service/session/session.service';
import { ProgressSpinnerModule } from 'primeng/primeng';
import { HttpTalkerService } from '../../service/http-talker/http-talker-service';
import { httpTalkServiceFactory } from '../../factories/http-talk-service-factory';



@NgModule({
    imports: [
        FormsModule,
        HttpModule,
        BrowserAnimationsModule,
        ProductModule,
        DashboardModule,
        TableModule,
        DataTableModule,
        ScrollPanelModule,
        CommonModule,
        HttpClientModule, 
        RouterModule,
        ProgressSpinnerModule,
        NgxSpinnerModule
    ],
    declarations: [
        MainComponent,
        MainMenuComponent,
        MainSubMenuComponent,
        MainTopbarComponent,
        MainFooterComponent,
        BreadcrumbComponent,
        MainRightpanelComponent,
        MainInlineProfileComponent,
    ],
    providers: [
        BreadCrumbService,
        ProductService,
        AuthService,
        LocalStorageService,
        SessionService,
        AuthenticationGuard,
        HttpTalkerService,
        NgxSpinnerService,{
            provide: HttpTalkerService,
            useFactory: httpTalkServiceFactory,
            deps: [XHRBackend, RequestOptions, NgxSpinnerService ]  
        }
    ]
})
export class MainModule { }
