import { ProductComponent } from './../product/list-products/product.component';
import { NotFoundComponent } from './../404-page/notFound.component';
import { LoginComponent } from './../login/login.component';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { AuthenticationGuard } from './../../guards/authentication.guard';


const appRoutes: Routes = [
    { path: '', component:ProductComponent },
    {path: '**', component: NotFoundComponent, pathMatch:'prefix'}];



@NgModule({
    imports: [RouterModule.forChild(appRoutes)],
    exports: [RouterModule]
})
export class MainRoutingModule { }