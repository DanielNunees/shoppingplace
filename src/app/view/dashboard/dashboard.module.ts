import { DashboardService } from './../../service/dashboard/dashboard.service';
import { TableModule } from 'primeng/table';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { EditorModule } from 'primeng/editor';
import { DataGridModule } from 'primeng/datagrid';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//PrimeNg Modules
import { InputTextModule, CarouselModule, KeyFilterModule, MultiSelectModule, DropdownModule, FileUploadModule, SpinnerModule, ProgressSpinnerModule, StepsModule, FieldsetModule, CardModule, MessageModule, GrowlModule, GalleriaModule, BlockUIModule, PanelModule, ConfirmDialogModule, ConfirmationService, CheckboxModule, ChartModule, ScheduleModule } from 'primeng/primeng';

//import { CarouselModule } from 'ngx-bootstrap/carousel';

//Components
import { DashboardComponent } from './dashboard.component';

//Services
import { ProductService } from '../../service/product/product.service';
import { AuthenticationGuard } from '../../guards/authentication.guard';
import { AuthService } from './../../service/authentication/auth.service';

//Modules
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardRoutingModule } from './dashboard.routing.module';





@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        DashboardRoutingModule,
        CheckboxModule,
        ChartModule,
        TableModule,
        DataGridModule,
        EditorModule,
        InputTextModule,
        ScheduleModule,
        KeyFilterModule,
        MultiSelectModule,
        DropdownModule,
        FileUploadModule,
        ReactiveFormsModule,
        SpinnerModule,
        ProgressSpinnerModule,
        StepsModule,
        FieldsetModule,
        CardModule,
        MessageModule,
        GrowlModule,
        ScrollPanelModule,
        GalleriaModule,
        ConfirmDialogModule,
        BlockUIModule,
        PanelModule,
        ProgressSpinnerModule,
        CarouselModule,
    ],
    declarations: [
        DashboardComponent,
    ],
    providers: [
        DashboardService,
        AuthenticationGuard,
        AuthService
    ],
})
export class DashboardModule { }
