import { DashboardComponent } from './dashboard.component';
import { NotFoundComponent } from './../404-page/notFound.component';
import { MainComponent } from './../main/main.component';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { AuthenticationGuard } from './../../guards/authentication.guard';

const dashboardRoutes: Routes = [
    {
        path: '', component: MainComponent,
        children: [{ path: 'dashboard', component: DashboardComponent, canActivate: [AuthenticationGuard] },
                   ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(dashboardRoutes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule { }