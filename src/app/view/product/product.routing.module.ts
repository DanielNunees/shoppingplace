import { DetailProductComponent } from './detail-product/detail-product,component';
import { NotFoundComponent } from './../404-page/notFound.component';
import { NewProductComponent } from './new-product/newproduct.component';
import { ProductComponent } from './list-products/product.component';
import { MainComponent } from './../main/main.component';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { AuthenticationGuard } from './../../guards/authentication.guard';
import { EditProductComponent } from './edit-product/edit-product.component';

const productRoutes: Routes = [
    {
        path: 'product', component: MainComponent,
        children: [{ path: 'list', component: ProductComponent, canActivate: [AuthenticationGuard] },
                   { path: 'new', component: NewProductComponent, canActivate: [AuthenticationGuard] },
                   { path: 'update', component: NewProductComponent, canActivate: [AuthenticationGuard] },
                   ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(productRoutes)],
    exports: [RouterModule]
})
export class ProductRoutingModule { }