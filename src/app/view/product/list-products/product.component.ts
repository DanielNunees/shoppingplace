import { Subscription } from 'rxjs/Subscription';
import { ProductModel } from './../../../models/product.model';
import { ProductService } from './../../../service/product/product.service';
import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';

@Component({
    selector: 'product-list',
    templateUrl: './product.component.html'
})
export class ProductComponent implements OnInit {
    products: ProductModel[];
    subscript: Subscription;

    constructor(private productService: ProductService,
        private router: Router) { }

    ngOnInit(): void {
        this.getAll();
    }

    newOrEditProduct(id) {
        this.productService._productId = id;
        this.router.navigate(['product/update']);
    }

    getAll() {
        this.subscript = this.productService.getAll().subscribe(
            res => { this.products = res});
    }

    ngOnDestroy() {
        this.subscript.unsubscribe();
    }
}