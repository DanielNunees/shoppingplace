import { ConfirmationService, Message } from 'primeng/primeng';
import { ImageService } from './../../../../service/image/image.service';
import { ProductData } from './../../../../models/productData.model';
import { ProductCombinationCardData } from './../../../../models/productCombinationCardData.model';
import { ProductService } from './../../../../service/product/product.service';
import { Images, ProductModel } from './../../../../models/product.model';
import { ProductCombination } from './../../../../models/productCombination.model';

import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Component, OnInit, Input, ViewEncapsulation, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'product-combination-card',
    templateUrl: './product-combination-card.component.html',
    styleUrls: ['../../product.scss'],
    providers: [ConfirmationService],
    encapsulation: ViewEncapsulation.None,
})
export class ProductCombinationCardComponent implements OnInit {
    @Input() productInfo: any;  //The product description (name, description, category, brand, id, productId)
    @Input() productData: ProductData;
    @Input() productCombinationCardData: ProductCombinationCardData;
    @Input() newCombinationCreated: boolean;

    @Output() combinations = new EventEmitter();

    productCombination: ProductCombination[] = [];
    images: Images[] = [];
    uploadImages: Images[] = [];
    msgs: Message[] = [];
    subscript: Subscription;
    productCombinationForm: FormGroup;
    product: ProductModel;
    blockCombination: boolean = false;
    imageMin: boolean = true;


    constructor(
        private productService: ProductService,
        private imageService: ImageService,
        private formBuilder: FormBuilder,
        private router: Router,
        private confirmationService: ConfirmationService) { }

    ngOnInit() {
        this.initForm();
        console.log(this.productData);
        console.log(this.productInfo);
        console.log(this.productCombinationCardData);
    }


    saveCombination() {
        if (this.productCombinationCardData.index != undefined)
            this.combinations.emit({ update: this.isUpdate() });
    }

    initForm() {
        this.productCombinationForm = this.formBuilder.group({
            color: new FormControl(null, Validators.required),
            combination: this.formBuilder.array((this.isUpdate() && this.productCombinationCardData.combination !== undefined) ? [] : [this.initCombination()]),
        });

        if ((this.isUpdate() && this.productCombinationCardData.combination !== undefined)) {
            this.initProductCombination();
        }
    }

    initProductCombination() {
        this.productCombinationCardData.combination.forEach(combination => {
            this.addDefinedCombination(combination);
        });
        this.imagesFromEachColor(this.productCombinationCardData.combination[0]);
        this.productCombinationForm.patchValue({
            color: this.productData.colors.find(x => x.color == this.productCombinationCardData.color)
        });
    }

    imagesFromEachColor(combination: ProductModel) {
        combination.images.forEach(image => {
            if (combination.colors.id == image.color_id) {
                this.images.push(image);
            }

        });
        if (this.images.length == 1) {
            this.imageMin = false;
        }
    }


    initCombination() {
        return this.formBuilder.group({
            price: new FormControl('', Validators.required),
            size: new FormControl('', Validators.required),
            quantity: new FormControl('', Validators.required)
        });
    }

    defineCombination(product: ProductModel) {
        return this.formBuilder.group({
            id: new FormControl(product.id),
            price: new FormControl(product.price, Validators.required),
            size: new FormControl(product.size, Validators.required),
            quantity: new FormControl(product.quantity, Validators.required),
            created_at: new FormControl(product.created_at)
        });
    }

    addDefinedCombination(product: ProductModel) {
        const control = <FormArray>this.productCombinationForm.controls['combination'];
        control.push(this.defineCombination(product));
    }

    addCombination() {
        const control = <FormArray>this.productCombinationForm.controls['combination'];
        control.push(this.initCombination());
    }

    deleteCombination(event, combination) {
        this.confirmationService.confirm({
            message: 'Deseja Realmente Deletar essa combinação?',
            header: 'Confirmar',
            icon: 'fa fa-trash',
            accept: () => {
                this.removeCombination(event, combination);
                this.msgs = [{ severity: 'info', summary: 'Confirmado', detail: 'Combinação Deletada' }];
            },
            reject: () => {
                this.msgs = [{ severity: 'info', summary: 'Rejected', detail: 'You have rejected' }];
            }
        });

    }

    deleteImageConfirmation(img, imgArray) {
        this.confirmationService.confirm({
            message: 'Deseja Realmente Deletar essa imagem?',
            header: 'Confirmar',
            icon: 'fa fa-trash',
            accept: () => {
                this.deleteImage(img, imgArray);
                this.msgs = [{ severity: 'info', summary: 'Confirmado', detail: 'Imagem Deletada' }];
            },
            reject: () => {
                this.msgs = [{ severity: 'info', summary: 'Rejected', detail: 'Imagem não deletada' }];
            }
        });

    }

    removeCombination(event: any, product_combination_id) {
        const control = <FormArray>this.productCombinationForm.controls['combination'];
        this.productService.deleteCombination(control.at(product_combination_id).value.id).subscribe(
            res => {
                control.removeAt(product_combination_id);
            });
    }

    onUpload(event, formUpload) {
        const fd: FormData = new FormData();
        let file: File[] = event.files;
        for (let i = 0; i < event.files.length; i++) {
            fd.append('photo[]', file[i], file[i].name);
        }
        this.productService.postPhotos(fd)
            .subscribe(res => {
                let aux = res.json();
                aux = aux[0];
                formUpload.clear(); this.images.push(new Images(aux)); this.uploadImages.push(new Images(aux));
                if (this.images.length > 1) {
                    this.imageMin = true;
                }
            }, err => { this.msgs.push({ severity: 'error', summary: 'Problemas!', detail: 'Error ao carregar a foto!' }) });

    }

    showError() {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: 'Problemas!', detail: 'Você tem que subir uma foto pelo menos.' });
    }

    deleteImage(img, imgArray) {
        this.imageService.deleteImageCombination(img.id).subscribe(res => {
        })
        if (imgArray.indexOf(img) > -1) {
            imgArray.splice(imgArray.indexOf(img), 1);
        }
        if (imgArray.length == 1) {
            this.imageMin = false;
        }
    }

    isUpdate(): Boolean {
        if (this.router.isActive('product/update', true)) {
            return true;
        }
        return false;
    }

    createCombinations(productCombinationForm) {
        this.productCombination = [];
        console.log(this.productInfo)
        productCombinationForm.combination.forEach(combination => {
            this.productCombination.push(
                new ProductCombination(
                    this.productInfo.productId,
                    combination.id,
                    combination.size.id,
                    productCombinationForm.color.id,
                    this.productInfo.product.brand.id,
                    combination.price,
                    combination.quantity,
                    this.uploadImages,
                    combination.created_at));
        });
    }



    onSubmit(productCombinationForm) {
        if (productCombinationForm.valid) {
            this.createCombinations(productCombinationForm.value);

            this.subscript = this.productService
                .saveOrUpdateProductCombination(this.productCombination)
                .subscribe(res => {
                    this.blockCombination = true;
                    this.saveCombination();
                    //this.productCombinationForm.reset();

                },
                    err => { });
        }
    }


    ngOnDestroy() {
        //this.subscript.unsubscribe();
    }
    get price() { return this.productCombinationForm.get('price'); }
    get color() { return this.productCombinationForm.get('color'); }
    get size() { return this.productCombinationForm.get('size'); }
    get quantity() { return this.productCombinationForm.get('quantity'); }

}