import { ProductData } from './../../../models/productData.model';
import { Router } from '@angular/router';
import { ProductService } from './../../../service/product/product.service';
import { Images } from './../../../models/product.model';
import { Subscription } from 'rxjs';
import { Component, OnInit, Input } from '@angular/core';
import { ProductCombinationCardData } from '../../../models/productCombinationCardData.model';

@Component({
    selector: 'product-combination',
    templateUrl: './product-combination.component.html',
})
export class ProductCombinationComponent implements OnInit {
    @Input() productDescription: any;
    subscript: Subscription;
    images: Images[] = [];
    productData: ProductData;
    productCombinationCardData: ProductCombinationCardData[] = [];
    blocked: boolean = true;
    isUpdate: boolean = true;
    newCombinationCreated: boolean = false;
    blockNewCombination: boolean = false;
    constructor(private productService: ProductService,
        private router: Router) { }

    ngOnInit() {
        this.getProductData();
        this.getProductCombination();
        this.blockNewCombination = !this.isProductUpdate();
    }

    saveCombination(event) {
        this.blockNewCombination = false;
    }

    addCombination() {
        this.blockNewCombination = true; //desabilita botão de nova combinação
        this.productCombinationCardData.push(new ProductCombinationCardData(undefined, undefined, 0));
    }

    isProductUpdate(): boolean {
        if (this.router.isActive('product/update', true)) {
            return true;
        }
        return false;
    }


    getProductData() {
        this.subscript = this.productService.getProductData()
            .subscribe(res => {
                this.productData = res;
            });

    }
    getProductCombination() {
        this.productService.getProductCombinationById(this.productDescription.productId)
            .map(res => {
                Object.entries(res).forEach(item => {
                    this.productCombinationCardData.push(new ProductCombinationCardData(item[0], item[1]))
                });
                if (this.productCombinationCardData.length == 0) {
                    this.productCombinationCardData.push(new ProductCombinationCardData(undefined, undefined, 0));
                    this.blockNewCombination = true;
                }

            }).subscribe(res => { this.blocked = false; });
    }
}
