import { MenuItem } from 'primeng/primeng';
import { Component, OnInit} from '@angular/core';

@Component({
    selector: 'new-product',
    templateUrl: './newproduct.component.html',
    styleUrls: ['./newproduct.scss'],
})
export class NewProductComponent implements OnInit {
    items: MenuItem[];
    activeIndex: number;
    product: any;
    id: number;

    constructor() {
        this.activeIndex = 0;
        this.items = [{
            label: 'Dados Iniciais',
            command: (event: any) => {
                this.activeIndex = 0;
            }
        },
        {
            label: 'Combinações',
            command: (event: any) => {
                this.activeIndex = 1;
            }
        },
        {
            label: 'Finalizar',
            command: (event: any) => {
                this.activeIndex = 2;
            }
        }
        ];

    }
    ngOnInit() {
    }


    backStep() {
        if (this.activeIndex != 0)
            this.activeIndex--;
    }
    nextStep(event) {
        this.product = event;
        if (this.activeIndex < 2)
            this.activeIndex++;
    }

    ngOnDestroy() { }

}
