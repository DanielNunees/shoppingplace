import { ImageService } from './../../service/image/image.service';
import { ProductCombinationCardComponent } from './product-combination/product-card/product-combination-card.component';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { EditProductComponent } from './edit-product/edit-product.component';
import { ProductDescriptionComponent } from './product-description/product-description.component';
import { EditorModule } from 'primeng/editor';
import { DataGridModule } from 'primeng/datagrid';
import { DetailProductComponent } from './detail-product/detail-product,component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//PrimeNg Modules
import {  InputTextModule,CarouselModule, KeyFilterModule, MultiSelectModule, DropdownModule, FileUploadModule, SpinnerModule, ProgressSpinnerModule, StepsModule, FieldsetModule, CardModule, MessageModule, GrowlModule, GalleriaModule, BlockUIModule, PanelModule, ConfirmDialogModule, ConfirmationService } from 'primeng/primeng';

//import { CarouselModule } from 'ngx-bootstrap/carousel';

//Components
import { NewProductComponent } from './new-product/newproduct.component';
import { ProductComponent } from './list-products/product.component';
import { ProductCombinationComponent } from './product-combination/product-combination.component';

//Services
import { ProductService } from '../../service/product/product.service';
import { AuthenticationGuard } from '../../guards/authentication.guard';
import { AuthService } from './../../service/authentication/auth.service';

//Modules
import { ProductRoutingModule } from './product.routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';





@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ProductRoutingModule, //routing
        DataGridModule,
        EditorModule,
        InputTextModule,
        KeyFilterModule,
        MultiSelectModule,
        DropdownModule,
        FileUploadModule,
        ReactiveFormsModule,
        SpinnerModule,
        ProgressSpinnerModule,
        StepsModule,
        FieldsetModule,
        CardModule,
        MessageModule,
        GrowlModule,
        ScrollPanelModule,
        GalleriaModule,
        ConfirmDialogModule,
        BlockUIModule,
        PanelModule,
        CarouselModule
    ],
    declarations: [
        ProductComponent,
        NewProductComponent,
        DetailProductComponent,
        ProductCombinationComponent,
        ProductDescriptionComponent,
        EditProductComponent,
        ProductCombinationCardComponent
    ],
    providers: [
        ProductService,
        ImageService,
        AuthenticationGuard,
        ConfirmationService,
        AuthService
    ],
})
export class ProductModule { }
