import { Router } from '@angular/router';
import { ProductService } from './../../../service/product/product.service';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { Categories, Brands } from './../../../models/product.model';
import { Subscription } from 'rxjs';
import { Component, OnInit, ViewEncapsulation, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'product-description',
    templateUrl: './product-description.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./product-description.scss']
})
export class ProductDescriptionComponent implements OnInit {
    subscript: Subscription;
    productForm: FormGroup;
    categories: Categories[];
    brands: Brands[];

    productId: number;
    @Output() stepIndex = new EventEmitter();

    constructor(private productService: ProductService,
        private formBuilder: FormBuilder,
        private router: Router) {
    }
    ngOnInit() {
        this.verifyRoute();
        this.getProductData();

    }

    verifyRoute() {
        if (!this.isUpdate()) {
            this.router.navigate(['product/list']);
        }
    }

    isUpdate(): Boolean {
        if (this.router.isActive('product/update', true) && typeof this.productService._productId !== 'undefined') {
            this.getProductToUpdate();
            return true;
        } else if (this.router.isActive('product/new', true)) {
            return true;
        }
        return false;
    }


    getProductData() {
        this.subscript = this.productService.getProductData()
            .subscribe(res => { this.categories = res.categories, console.log(res), this.brands = res.brands })
        //.subscribe(res => {  console.log(res)});

        this.productForm = this.formBuilder.group({
            id: new FormControl(null),
            name: new FormControl(null, Validators.required),
            description: new FormControl(null, Validators.required),
            category: new FormControl(null, Validators.required),
            brand: new FormControl(null, Validators.required)
        })

    }

    getProductToUpdate() {
        this.productService.getProductDescriptionById(this.productService._productId)
            .subscribe(res => {
                console.log(res);
                this.productForm.patchValue({
                    id: this.productService._productId,
                    name: res.name,
                    description: res.description,
                    brand: res.brands[0],
                    category: [res.categories[0]]
                }),
                    this.productService._productId = undefined;
            });

    }

    onSubmit(form) {
        this.subscript = this.productService
            .saveProduct(form.value)
            .subscribe(res => {
                this.stepIndex.emit({ stepIndex: 1, productId: res.json(), product: form.value });
                this.productForm.reset();
            },
                err => { });
    }


    ngOnDestroy() {
        this.subscript.unsubscribe();
    }
    get name() { return this.productForm.get('name'); }
    get description() { return this.productForm.get('description'); }
    get category() { return this.productForm.get('category'); }
    get brand() { return this.productForm.get('brand'); }

    set name(name) { this.productForm.get('name') }
    set description(description) { this.productForm.get('description') }
    set category(category) { this.productForm.get('category') }
    set brand(category) { this.productForm.get('brand') }

}