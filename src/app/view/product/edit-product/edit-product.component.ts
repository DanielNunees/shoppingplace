import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ProductModel, Categories, Sizes, Colors } from './../../../models/product.model';
import { ProductService } from './../../../service/product/product.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { asTextData } from '@angular/core/src/view';


@Component({
    templateUrl: './edit-product.component.html',
})

export class EditProductComponent implements OnInit {
    id: string;
    subscript: Subscription;
    productEdit: ProductModel;

    constructor(private productService: ProductService,
        private router: ActivatedRoute,
        private formBuilder: FormBuilder) {
    }
    ngOnInit() {
        this.subscript = this.router.params
            .subscribe((params: any) => {
                this.id = params['id'];
            });
    }

    ngOnDestroy() {
    }
}