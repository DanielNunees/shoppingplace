import { AppSettings } from './../../app.settings';
import { HttpTalkerService } from '../http-talker/http-talker-service';
import { LocalStorageService } from './../local-storage/local-storage.service';
import { LoginDataForm } from './../../view/login/login.component';
import { OnInit, Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';

@Injectable()
export class AuthService implements OnInit {
    private _authenticated: boolean = false;
    private authURL = AppSettings.API_ENDPOINT + "auth/login"; // Passport authentication URL
    private logoutURL = AppSettings.API_ENDPOINT + "auth/logout"; // Passport authentication URL
    private authenticatedUserURL = AppSettings.API_ENDPOINT + "auth/isAuthenticated" // Is user Authenticated
    private headers = new Headers(); // headers for each request
    private options = new RequestOptions({ headers: this.headers });
    private postData = {
        email: "", // an User in Laravel database
        password: "" // the user's password
    }
    constructor(private httpService: HttpTalkerService,
        private localStorageService: LocalStorageService,
        private router: Router) {
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
    }

    ngOnInit(): void {
    }

    login(user: LoginDataForm) {

        this.postData.password = user.password;
        this.postData.email = user.username;

        return this.httpService.httpPost(this.authURL, this.postData, this.options)
            .map(response => this.setToken(response.json().access_token)
            );
    }

    logout() {
        this.httpService.httpPost(this.logoutURL, null).subscribe(
            res => {
                this.router.navigate(['login']);
                this.localStorageService.removeItem("_token");
                this._authenticated = false;
            });
    }

    setToken(token) {

        this.localStorageService.setItem("_token", token);  // save the access_token in local Storage
        this._authenticated = true;
    }

    /**
     * Informa para a aplicacação se o usuario esta autenticado
     */

    isAuthenticated() {
        return this.httpService.httpPost(this.authenticatedUserURL, null).map(
            result => {

                if (result.ok) return true;

                else return false;
            },
            error => {
                if (error.status == 401) {
                    this.localStorageService.removeItem("_token");
                    this.router.navigate(['/login']);
                    return false;
                }
            }
        );
    }

}