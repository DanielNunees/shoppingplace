import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import {
    Http,
    RequestOptionsArgs,
    Response,
    Headers,
    XHRBackend
} from '@angular/http';

import { AngularReduxRequestOptions } from './angular-redux-request.options';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class HttpTalkerService extends Http {

    requests: Array<number> = [];

    constructor(
        backend: XHRBackend,
        defaultOptions: AngularReduxRequestOptions,
        private loaderService: NgxSpinnerService
    ) {
        super(backend, defaultOptions);
    }

    httpGet(url: string, options?: RequestOptionsArgs): Observable<any> {

        this.showLoader();
        this.requests.push(1);
        return super.get(url, this.requestOptions(options))
            .catch(this.onCatch)
            .do((res: Response) => {
                this.onSuccess(res);
            }, (error: any) => {
                this.onError(error);
            })
            .finally(() => {
                this.requests.pop();
                this.onEnd();
            });

    }


    httpPost(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {

        this.showLoader();
        this.requests.push(1);
        return super.post(url, body, this.requestOptions(options))
            .catch(this.onCatch)
            .do((res: Response) => {
                this.onSuccess(res);
            }, (error: any) => {
                this.onError(error);
            })
            .finally(() => {
                this.requests.pop();
                this.onEnd();
            });

    }

    uploadFile(url: string, body: any): Observable<Response> {
        return super.post(url, body, {
            headers: this.getAuthorizationHeadersToUploadFiles()
        });
    }

    private requestOptions(options?: RequestOptionsArgs): RequestOptionsArgs {

        if (options == null) {
            options = new AngularReduxRequestOptions();
        }

        if (options.headers == null) {
            options.headers = new Headers();
        }

        return options;
    }

    /**
* Retorna os cabeçalhos para upload de arquivos e de conexão com o servidor.
*/
    private getAuthorizationHeadersToUploadFiles(): Headers {
        let headers: Headers = new Headers();
        let token: string = localStorage.getItem("_token");
        headers.append('Authorization', 'Bearer ' + token); // add the Authentication header
        return headers;
    }

    private onCatch(error: any, caught: Observable<any>): Observable<any> {
        return Observable.throw(error);
    }

    private onSuccess(res: Response): void {
    }

    private onError(res: Response): void {
    }

    private onEnd(): void {
        this.hideLoader();
    }

    private showLoader(): void {
        this.loaderService.show();
    }

    private hideLoader(): void {
        if (this.requests.length == 0)
            this.loaderService.hide();
    }
}