import { BaseRequestOptions } from '@angular/http';

export class AngularReduxRequestOptions extends BaseRequestOptions {

    public token: string;

    constructor(angularReduxOptions?: any) {

        super();
        this.token = localStorage.getItem("_token");

        this.headers.append('Authorization', 'Bearer ' + this.token); // add the Authentication header
        this.headers.append('authDevice', this.getDevice());
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');


        if (angularReduxOptions != null) {

            for (let option in angularReduxOptions) {
                let optionValue = angularReduxOptions[option];
                this[option] = optionValue;
            }
        }
    }
    //DeviceInfoService
    /**
     * Descobre se o dispositivo é mobile ou não.
     */
    public isMobile(): boolean {
        let userAgent = navigator.userAgent.toLowerCase();
        if (userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i) != -1)
            return true;
        return false;
    }

    /**
     * Retorna o nome do browser que está sendo utilizado.
     */
    public getBrowser(): any {
        let ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return { name: 'IE', version: (tem[1] || '') };
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\Edge\/(\d+)/)
            if (tem != null) {
                { return { name: 'Edge', version: tem[1] }; }
            }
            tem = ua.match(/\bOPR\/(\d+)/)
            if (tem != null) {
                { return { name: 'Opera', version: tem[1] }; }
            }
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) != null) { M.splice(1, 1, tem[1]); }
        return {
            name: M[0],
            version: M[1]
        };
    }

    /**
     * Retorna o tipo de dispositivo que está sendo usado.
     */
    public getDevice(): string {
        let authDevice: string = 'Dispositivo: ';
        //Verificação de tipo de dispositivo
        if (this.isMobile()) authDevice = authDevice + 'Smartphone. ';
        else authDevice = authDevice + 'PC. ';
        //Verificação de qual navegador
        authDevice = authDevice + 'Navegador: ' + this.getBrowser().name + '.';
        return authDevice;
    }

}