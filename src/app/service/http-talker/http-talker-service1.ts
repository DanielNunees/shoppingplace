import { LocalStorageService } from './../local-storage/local-storage.service';
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ProfileModel } from '../../models/profile.model';
import { SessionService } from '../session/session.service';

/**
 * Classe responsável por gerenciar as requisições básicas para camada de serviço.
 * @author Daniel R.
 */
@Injectable()
export class HttpTalkerService1 {

  constructor(
    private http: Http,
    private sessionService: SessionService,
    private localStorageService: LocalStorageService
  ) { }

  /**
   * Retorna um Observable de uma requisição GET.
   */
  httpGet(url: string): Observable<Response> {
    return this.http.get(url, {
      headers: this.getAuthorizationHeaders()
    });


  }

  /** 
   * Retorna um Observable de uma requisição POST.
   */
  httpPost(url: string, body: any): Observable<Response> {
    return this.http.post(url, body, {
      headers: this.getAuthorizationHeaders()
    });
  }

  /** 
   * Retorna um Observable de uma requisição PUT.
   */
  httpPut(url: string, body: any): Observable<Response> {
    return this.http.put(url, body, {
      headers: this.getAuthorizationHeaders()
    });
  }

  /** 
   * Retorna um Observable de uma requisição DELETE.
   */
  httpDelete(url: string): Observable<Response> {
    return this.http.delete(url, {
      headers: this.getAuthorizationHeaders()
    });
  }

  uploadFile(url: string, body: any): Observable<Response>{
    return this.http.post(url, body, {
      headers: this.getAuthorizationHeadersToUploadFiles()
    });
  }

  /** 
   * Retorna um Observable de uma requisição POST de Login.
   */
  login(url: string, base64: string): Observable<Response> {
    return this.http.post(url, '', {
      headers: this.getAuthenticationLoginHeaders(base64)
    });
  }

  /**
   * Retorna um Observable de uma requisição POST de Logout.
   */
  logout(url: string) {
    return this.http.post(url, '', {
      headers: this.getAuthenticationLogoutHeaders()
    });
  }

  /**
   * Retorna os cabeçalhos padrão de conexão com o servidor.
   */
  private getAuthorizationHeaders(): Headers {
    let headers: Headers = new Headers();
    let token: string = this.localStorageService.getItem("_token");

    headers.append('Authorization', 'Bearer ' + token); // add the Authentication header
    headers.append('authDevice', this.getDevice());
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');

    return headers;
  }

  /**
 * Retorna os cabeçalhos para upload de arquivos e de conexão com o servidor.
 */
  private getAuthorizationHeadersToUploadFiles(): Headers {
    let headers: Headers = new Headers();
    let token: string = this.localStorageService.getItem("_token");
    headers.append('Authorization', 'Bearer ' + token); // add the Authentication header
    return headers;
  }

  private sendHeaders(headers: Headers): Headers {
    return headers;
  }

  /**
   * Cria os cabeçalhos padrão para comunicação com o servidor.
   */
  private getAuthenticationLoginHeaders(base64: string): Headers {
    let headers: Headers = new Headers();

    headers.append('Authorization', 'Basic ' + base64);
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    headers.append('authDevice', this.getDevice());

    return headers;
  }

  /**
   * Cria os cabeçalhos padrão para comunicação com o servidor.
   */
  private getAuthenticationLogoutHeaders(): Headers {
    let headers: Headers = new Headers();
    let profile: ProfileModel = this.sessionService.getItem('profile');

    headers.append('authUsername', profile.username);
    headers.append('authToken', profile.token);
    headers.append('authDevice', this.getDevice());

    return headers;
  }
  //DeviceInfoService
  /**
   * Descobre se o dispositivo é mobile ou não.
   */
  public isMobile(): boolean {
    let userAgent = navigator.userAgent.toLowerCase();
    if (userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i) != -1)
      return true;
    return false;
  }

  /**
   * Retorna o nome do browser que está sendo utilizado.
   */
  public getBrowser(): any {
    let ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
      tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
      return { name: 'IE', version: (tem[1] || '') };
    }
    if (M[1] === 'Chrome') {
      tem = ua.match(/\Edge\/(\d+)/)
      if (tem != null) {
        { return { name: 'Edge', version: tem[1] }; }
      }
      tem = ua.match(/\bOPR\/(\d+)/)
      if (tem != null) {
        { return { name: 'Opera', version: tem[1] }; }
      }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) { M.splice(1, 1, tem[1]); }
    return {
      name: M[0],
      version: M[1]
    };
  }

  /**
   * Retorna o tipo de dispositivo que está sendo usado.
   */
  public getDevice(): string {
    let authDevice: string = 'Dispositivo: ';
    //Verificação de tipo de dispositivo
    if (this.isMobile()) authDevice = authDevice + 'Smartphone. ';
    else authDevice = authDevice + 'PC. ';
    //Verificação de qual navegador
    authDevice = authDevice + 'Navegador: ' + this.getBrowser().name + '.';
    return authDevice;
  }
}
