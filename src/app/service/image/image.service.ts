import { Images } from './../../models/product.model';
import { HttpTalkerService } from '../http-talker/http-talker-service';
import { AppSettings } from './../../app.settings';
import { Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class ImageService {
    _productId: number;
    images: Images;

    self = this;
    constructor(private httpTalker: HttpTalkerService) {

    }


    deleteImageCombination(photo_id: any) {
        return this.httpTalker.httpPost(AppSettings.API_ENDPOINT + "image/deleteImageCombination", {id:photo_id})
            .map((res: Response) => { return res });
    }


}