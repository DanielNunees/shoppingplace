import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

  constructor() { }
  /**
   * Salva os dados no localstorage do browser.
   *
   * @param key Identidicador único que será usado posteriormente para recuperar o valor salvo no localstorage.
   * @param value Valor que será gravado no localstorage, esse valor poder ser um Objeto qualquer, ou uma string.
   */
  setItem(key: string, value: Object | string | any): void {
    localStorage.setItem(key, typeof value === 'string' ? value : JSON.stringify(value));
  }

  /**
   * Recuperar o valor de um objeto do localstorage de acordo com a chave informada.
   *
   * @param key Chave que o valor está associado. Essa chave deve ser a mesma que foi informada quando o valor foi salvo no localstorage.
   * @return Objeto associado a chave informada no parâmetro da função.
   */
  getItem(key: string): Object | any {
    let object = localStorage.getItem(key);
    return object ;
  }

  /**
   * Remove um valor do localstorage.
   *
   * @param key Chave que tera seu valor removido.
   */
  removeItem(key: string): void {
    localStorage.removeItem(key);
  }
  /**
   * Verifica se existe algum valor para a chave informada.
   *
   * @param key Chave que tera seu valor consultado.
   */
  hasItem(key: string): boolean {
    return localStorage.getItem(key) !== null;
  }
  /**
   * Limpa todos os dados do localstorage. Limpa somente os dados que foram adicionados pelo sistema.
   */
  clear(): void {
    localStorage.clear();
  }
}