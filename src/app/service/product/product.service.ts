import { HttpTalkerService } from '../http-talker/http-talker-service';
import { AppSettings } from './../../app.settings';
import { Response } from '@angular/http';
import { ProductModel } from './../../models/product.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { ProductCombination } from '../../models/productCombination.model';
import { ProductData } from '../../models/productData.model';

@Injectable()
export class ProductService {
    products: ProductModel[] = [];
    _productId: number;

    self = this;
    constructor(private httpTalker: HttpTalkerService ) {

    }

    getAll(): Observable<ProductModel[]> {

        return this.httpTalker.httpGet(AppSettings.API_ENDPOINT + 'product/products')
            .map((res: Response) => {
                let products = res.json() || [];
                return products.map(product => this.jsonToProductModel(product));
            });
    }

    getProductDescriptionById(id: number): Observable<ProductModel> {
        return this.httpTalker.httpGet(AppSettings.API_ENDPOINT + "product/productDescription/" + id)
            .map((res: Response) => {
                return Object.assign(new ProductModel(), res.json());
            })
    }

    getProductCombinationById(id: number): Observable<{}> {
        return this.httpTalker.httpGet(AppSettings.API_ENDPOINT + "product/productCombination/" + id)
            .map((res: Response) => {
                let products = res.json() || [];

                return this.jsonToProductMapModel(products);
            })
    }


    getProductData(): Observable<ProductData> {
        return this.httpTalker.httpGet(AppSettings.API_ENDPOINT + "product/newProduct")
            .map((res: Response) => {
                return Object.assign(new ProductData(), res.json());
            })
    }

    saveProduct(product: ProductModel) {
        return this.httpTalker.httpPost(AppSettings.API_ENDPOINT + "product/saveProduct", product)
            .map((res: Response) => { return res });
    }

    saveOrUpdateProductCombination(product: ProductCombination[]) {
        return this.httpTalker.httpPost(AppSettings.API_ENDPOINT + "product/saveOrUpdateProductCombination", product)
            .map((res: Response) => { return res });
    }

    postPhotos(photos: any) {
        return this.httpTalker.uploadFile(AppSettings.API_ENDPOINT + "image/savePhotos", photos)
            .map((res: Response) => { return res });
    }

    deleteCombination(combination_id) {
        return this.httpTalker.httpPost(AppSettings.API_ENDPOINT + "product/deleteProductCombination", { "combination_id": combination_id })
            .map((res: Response) => { return res });
    }


    get productId() { return this._productId }
    set productId(id: number) { this._productId = id; }

    /**
 * Converte um objeto no modelo enviado pelo backend para o modelo utilizado na aplicação.
 * @param json Objeto no formato Json
 */
    jsonToProductModel(json: any): ProductModel {
        let product: ProductModel = Object.assign(new ProductModel(), json);
        return product;
    }

    jsonToProductMapModel(products: any) {
        let aux2 = [];
        let aux = {};
        var map = new Map(Object.entries(products));
        map.forEach(function (value, key) {
            aux2 = [];
            value.forEach(item => {
                aux2.push(this.jsonToProductModel(item));

            });
            aux[key] = aux2;
        }, this);
        return aux;
    }
}