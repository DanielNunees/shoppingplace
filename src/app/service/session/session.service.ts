import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {

  constructor() { }

  setItem(key: string, value: Object | string | any): void {
    sessionStorage.setItem(key, typeof value === "string" ? value : JSON.stringify(value));
  }

  getItem(key: string): Object | any {
    let object = sessionStorage.getItem(key);
    return object ? JSON.parse(sessionStorage.getItem(key)) : null;
  }

  removeItem(key: string): void {
    sessionStorage.removeItem(key);
  }

  hasItem(key: string): boolean {
    return sessionStorage.getItem(key) !== null;
  }

  clear(): void {
    sessionStorage.clear();
  }

}
