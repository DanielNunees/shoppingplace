import { Observable } from 'rxjs/Observable';
import { AppSettings } from './../../app.settings';
import { HttpTalkerService } from '../http-talker/http-talker-service';
import { Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class DashboardService {
    _productId: number;

    self = this;
    constructor(private httpTalker: HttpTalkerService) {

    }



    getUser(){
        return this.httpTalker.httpGet(AppSettings.API_ENDPOINT + "dashboard/dashboard")
            .map((res: Response) => {
                return res.json();
            })
    }


}