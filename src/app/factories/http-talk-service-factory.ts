import { NgxSpinnerService } from 'ngx-spinner';
import { AngularReduxRequestOptions } from './../service/http-talker/angular-redux-request.options';
import { HttpTalkerService } from '../service/http-talker/http-talker-service';
import { XHRBackend } from '@angular/http';
function httpTalkServiceFactory(backend: XHRBackend, options: AngularReduxRequestOptions, loaderService: NgxSpinnerService ) {
    return new HttpTalkerService(backend, options, loaderService);
}
export { httpTalkServiceFactory };