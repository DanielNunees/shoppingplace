import { AuthService } from './../service/authentication/auth.service';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { SessionService } from '../service/session/session.service';

/**
 * Guarda utilizada para verificar se um usuário está autenticado no sistema ou não.
 * @author Daniel R.
 */
@Injectable()
export class AuthenticationGuard implements CanActivate {

  constructor(
    private router: Router,
    private sessionService: SessionService,
    private authService: AuthService,
  ) { }

  /**
   * Método que define se a rota solicitada pode ser ativada ou não.
   * Retorna true se for o usuário estiver autenticado e false caso contrário. 
   */
  // canActivate(
  //   route: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot
  // ): Observable<boolean> | boolean {
    
  //   return this.authService.isAuthenticated();

  // }


  canActivate(next:ActivatedRouteSnapshot, state:RouterStateSnapshot) {
    return this.authService.isAuthenticated().map(e => {
        if (e) {
            return true;
        }
    }).catch(() => {
        this.router.navigate(['/login']);
        return Observable.of(false);
    });
}   

}
